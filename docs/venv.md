venv script
-------------------------------------------------------------------------------

## Variables

| Variable glob or name |   Default Value    | Description |
|-----------------------|:------------------:|-------------|
| VENV_HYPER_*          | -                  | Hypervisor specific variables. | 
| VENV_HYPER_BOX_*      | -                  | Main virtual machine specific variables. |
| VENV_HYPER_BOX_CPU    | 4                  | Number of cpu required for the main virtual machine. |
| VENV_HYPER_BOX_IMG    | ubuntu/bionic64    | Image for the main and all virtual machines. |
| VENV_HYPER_BOX_MEM    | 4096               | Memory size required for the main virtual machine. |
| VENV_HYPER_BOX_NAME   | -                  | Box configuration (image/version) friendly name. |
| VENV_HYPER_BOX_NUM    | 1                  | Number of instance required (not used here). |
| VENV_HYPER_BOX_SSH    | 127.0.0.1          | External ssh access (remote provisionning) when 0.0.0.0 is used. |
| VENV_HYPER_BOX_VER    | >= 0               | Image version for the main and all virtual machines. |
| VENV_HYPER_NET_*      |                    | Variables synchonizing network configuration beetween hypervisor and playbooks. |
| VENV_HYPER_NET_FQDN   | $(hostname -f)     | Network domain name, computed by default by venv script. |
| VENV_HYPER_NET_PORT   | 8443               | Main virtual machine forwarded port. |
| VENV_PROXY_REPO       |                    | Generic package manager repository (NXRM) url used as upstream. |
| VENV_PROXY_DOCKER     |                    | Docker hub to use inside the building infrastructure (to avoid docker pull limits). |
| VENV_REPO_GIT_PULL    | https://gitlab.com | Git server url used to pull repositories. |
| VENV_REPO_GIT_BRANCH  | master             | Git branch used to pull repositories. |
| VENV_SCRIPT_*         | -                  | Script specific variables. |
| VENV_SCRIPT_HOME      | .                  | Python virtual environment path. | 
| VENV_SCRIPT_PATH      | .                  | Sourced script path (venv bash script path). |
| VENV_SCRIPT_PROJ      | basename $(pwd)    | venv project name, used to create project environment. |
